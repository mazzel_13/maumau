#include "precompiled.h"

// Constructors
// -- do not use this default constructor!
CPlayingCard::CPlayingCard( void ) {
}

// -- always use this constructor instead!
CPlayingCard::CPlayingCard( int _suit, int _value ) {
	suit = _suit;
	value = _value;
}

// Destructor
CPlayingCard::~CPlayingCard( void ) {
}

// Getter
int CPlayingCard::GetSuit( void ) {
	return ( suit );
}

int CPlayingCard::GetValue( void ) {
	return ( value );
}

// Setter

// Remaining public methods
bool CPlayingCard::IsValid( bool jackIsPlayed, int requestedSuit, CPlayingCard openCard ) {
	bool return_value = false;

	if ( jackIsPlayed || value == openCard.GetValue() || suit == openCard.GetSuit() || value == JACK ) {
		return_value = true;
	}
	if ( jackIsPlayed && suit != requestedSuit ) {
		return_value = false;
	}
	if ( value == openCard.GetValue() && value == 11 ) {
		return_value = false;
	}
	
	return ( return_value );
}

// Remaining private methods