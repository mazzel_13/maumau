#pragma once

class CPlayer {
public:
							CPlayer( void );
							CPlayer( bool _isHuman, int _id );

							~CPlayer( void );

	string					GetName( void );
	vector<CPlayingCard>	GetHand( void );
	
	void					ExpandHand( CPlayingCard card );
	void					SetName( string _name );
	
	void					Turn( bool jackIsPlayed, int jackOwner, int language, int requestedSuit, int specialCard, CPlayingCard openCard );

private:
	bool					isHuman;
	int						id;
	string					name;
	unsigned int			lineCnt;
	vector<CPlayingCard>	hand;
	
	void					ConfirmToProceed( int language );					
	void					DrawCards( unsigned int cnt );
	void					IncreaseLineCnt( unsigned int cnt );
	void					PlayOneCard( int card, int language );
	void					PrintOneCard( CPlayingCard card, int language );
	void					SpecialAction( int jackOwner, int language, int requestedSuit, int specialCard, int wertOffeneKarte );
	void					SpecialCard( int cardValue, int language );
};