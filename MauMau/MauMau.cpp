#include "precompiled.h"
#include <cstdlib>
#include <ctime>

// main function
int main( void ) {
	return ( 0 );
}

// Initializing a game object to work with
CMauMau game;

// Constructors
CMauMau::CMauMau( void ) {
	language = EN;
	countOfOpponents = 2;
	handSize = 5;
	activePlayer = 0;

	specialCard = 0;
	requestedSuit = 0;
	jackIsPlayed = false;

	clear();

	StartGame();
}

// Destructor
CMauMau::~CMauMau( void ) {
}

// Getter
vector<CPlayer> CMauMau::GetPlayers( void ) {
	return ( players );
}

vector<CPlayingCard> CMauMau::GetDrawingStack( void ) {
	return ( drawingStack );
}

// Setter
void CMauMau::ExpandOpenStack( CPlayingCard card ) {
	openStack.push_back( card );
	
	return;
}

void CMauMau::ReduceDrawingStack( void ) {
	drawingStack.pop_back();
	if ( drawingStack.size() == 0 ) {
		SwapStacks();
	}

	return;
}

void CMauMau::SetJack( bool state, int owner ) {
	jackIsPlayed = state;
	jackOwner = owner;
}

void CMauMau::SetRequestedSuit( int suit ) {
	requestedSuit = suit;
	
	return;
}

void CMauMau::SetSpecialCard( int card ) {
	specialCard = card;
	
	return;
}

// Remaining public methods
void CMauMau::PrintOneCard( CPlayingCard card ) {
	if ( language == EN ) {
		switch ( card.GetValue() ) {
		case 7:
			cout	<< "Seven";
			break;
		case 8:
			cout	<< "Eight";
			break;
		case 9:
			cout	<< "Nine";
			break;
		case 10:
			cout	<< "Ten";
			break;
		case JACK:
			cout	<< "Jack";
			break;
		case QUEEN:
			cout	<< "Queen";
			break;
		case KING:
			cout	<< "King";
			break;
		case ACE:
			cout	<< "Ace";
			break;
		default:
			break;
		}
		cout	<< " of ";
		switch ( card.GetSuit() ) {
		case SPADES:
			cout	<< "Spades";
			break;
		case HEARTS:
			cout	<< "Hearts";
			break;
		case DIAMONDS:
			cout	<< "Diamonds";
			break;
		case CLUBS:
			cout	<< "Clubs";
			break;
		default:
			break;
		}
	} else {
		switch ( card.GetSuit() ) {
		case SPADES:
			cout	<< "Pik";
			break;
		case HEARTS:
			cout	<< "Herz";
			break;
		case DIAMONDS:
			cout	<< "Karo";
			break;
		case CLUBS:
			cout	<< "Kreuz";
			break;
		default:
			break;
		}
		cout	<< "-";
		switch ( card.GetValue() ) {
		case 7:
			cout	<< "Sieben";
			break;
		case 8:
			cout	<< "Acht";
			break;
		case 9:
			cout	<< "Neun";
			break;
		case 10:
			cout	<< "Zehn";
			break;
		case JACK:
			cout	<< "Bube";
			break;
		case QUEEN:
			cout	<< "Dame";
			break;
		case KING:
			cout	<< "Koenig";
			break;
		case ACE:
			cout	<< "Ass";
			break;
		default:
			break;
		}
	}
	
	return;
}

void CMauMau::PrintStack( vector<CPlayingCard> stack ) {
	for ( unsigned int i = 0; i < stack.size(); i++ ) {
		PrintOneCard( stack.at( i ) );
		cout	<< endl;
	}
	
	return;
}

// Remaining private methods
bool CMauMau::GameStillRunning( void ) {
	bool return_value = true;

	for ( unsigned int i = 0; i < players.size(); i++ ) {
		if ( players.at( i ).GetHand().size() == 0 ) {
			return_value = false;
		}
	}
	
	return ( return_value );
}

bool CMauMau::RandomNumbersEqual( void ) {
	if ( random1 == random2 ) {
		return ( true );
	} else if ( random1 == random3 ) {
		return ( true );
	} else if ( random1 == random4 ) {
		return ( true );
	} else if ( random2 == random3 ) {
		return ( true );
	} else if ( random2 == random4 ) {
		return ( true );
	} else if ( random3 == random4 ) {
		return ( true );
	} else {
		return ( false );
	}
}

void CMauMau::AnnounceWinner( void ) {
	int input = 0;
	unsigned int winner = 0;

	if ( activePlayer == 0 ) {
		winner = players.size();
		winner--;
	} else {
		winner = activePlayer - 1;
	}

	PrintHeader();

	if ( language == EN ) {
		if ( winner == 0 ) {
			cout	<< " Congratulations! You won the game!" << endl;
			cout	<< " I hope, you had fun." << endl;
		} else {
			cout	<< " Unfortunately, you didn't win." << endl;
			cout	<< " Player" << winner + 1 << " is the winner." << endl;
		}
	} else {
		if ( winner == 0 ) {
			cout	<< " Herzlichen Glueckwunsch! Du hast das Spiel gewonnen!" << endl;
			cout	<< " Ich hoffe sehr, es hat dir Spass gemacht." << endl;
		} else {
			cout	<< " Du hast leider nicht gewonnen." << endl;
			cout	<< " Der Sieger heisst Spieler" << winner + 1 << "." << endl;
		}
	}

	if ( language == EN ) {
		cout	<< endl << " Wanna play again? " << endl;
		cout	<< "  [1] Yes." << endl;
		cout	<< "  [0] No." << endl;
	} else {
		cout	<< endl << " Moechtest du erneut spielen? " << endl;
		cout	<< "  [1] Ja." << endl;
		cout	<< "  [0] Nein." << endl;
	}
	cout	<< endl << "   ";
	cin		>> input;
	cin.get();

	// FALSE == 0, TRUE == 1
	if ( input < FALSE ) {
		input = FALSE;
	} else if ( input > TRUE ) {
		input = TRUE;
	}
	
	if ( input == FALSE ) {
		return;
	} else {
		clear();
		StartGame();
	}
}

void CMauMau::Deal( void ) {
	for ( unsigned int i = 0; i < handSize; i++ ) {
		for ( unsigned int i = 0; i < players.size(); i++ ) {
			players.at( i ).ExpandHand( drawingStack.back() );
			drawingStack.pop_back();
		}
	}

	return;
}

void CMauMau::PrintGameBoard( void ) {
	string name = players.at( activePlayer ).GetName();

	if ( activePlayer == 0 ) {
		if ( language == EN ) {
			cout	<< " " << name << ", it is your turn." << endl;
		} else {
			cout	<< " " << name << ", du bist an der Reihe." << endl;
		}
		cout	<< " ";
		for ( unsigned int i = 0; i < name.size(); i++ ) {
			cout	<< "-";
		}
		if ( language == EN ) {
			cout	<< "------------------" << endl << endl;
		} else {
			cout	<< "-----------------------" << endl << endl;
		}
	} else {
		if ( language == EN ) {
			cout	<< " It is Player" << ( activePlayer + 1 ) << "'s turn." << endl;
			cout	<< " ---------------------" << endl << endl;
		} else {
			cout	<< " Spieler" << ( activePlayer + 1 ) << " ist an der Reihe." << endl;
			cout	<< " --------------------------" << endl << endl;
		}
	}
	
	if ( language == EN ) {
		cout	<< " Number of cards on the drawing stack: " << drawingStack.size() << endl;
		cout	<< " Open card: \"";
	} else {
		cout	<< " Anzahl der Karten auf dem Stapel: " << drawingStack.size() << endl;
		cout	<< " Offene Karte: \"";
	}
	PrintOneCard( openStack.back() );
	cout	<< "\"" << endl << endl;

	return;
}

void CMauMau::GameLoop(  ) {
	while ( GameStillRunning() ) {
		PrintHeader();
		
		PrintGameBoard();

		players.at( activePlayer ).Turn( jackIsPlayed, jackOwner, language, requestedSuit, specialCard, openStack.back() );
		activePlayer += 1;
		if ( activePlayer >= players.size() ) {
			activePlayer = 0;
		}
		clear();
	}

	AnnounceWinner();
	
	return;
}

void CMauMau::PrintHeader( void ) {
	cout	<< endl <<	" +----------------------------------------------------------------------------+" << endl;
	cout	<< " |                  \"New Old Mau-Mau\", (c) 2013 Marcel Lasaj      v.1.01 BETA |" << endl;
	cout	<< " +----------------------------------------------------------------------------+" << endl << endl;
	
	return;
}

void CMauMau::PrintIntroduction( void ) {
	PrintHeader();
	
	if ( language == EN ) {
		cout	<< " Welcome!" << endl << endl;

		cout	<< " This is New Old Mau-Mau, a classic variant of the well known card game." << endl << endl;
		
		cout	<< " It's the goal of the game to get rid of all the cards in your hand." << endl;
		cout	<< " To reach this goal, each player gets one turn to play a card." << endl;
		cout	<< " Who's the first to get rid of his cards wins and, by that, ends the game." << endl << endl;
		
		cout	<< " You have to respect the following rules:" << endl;
		cout	<< "  - One can play a card if it corresponds to the suit (Spades, Hearts, ...)" << endl;
		cout	<< "    or to the value (7, 10, King, ...) of the open card." << endl;
		cout	<< "  - \"Jack on everything\": One may play a Jack on every card ..." << endl;
		cout	<< "  - \"Jack on Jack stinks\": ... every card except another Jack." << endl;
		cout	<< "  - Did the previous player play a 7, one has to draw two cards." << endl;
		cout	<< "  - In this case, one is not allowed to play another card." << endl;
		cout	<< "  - Also, it's not allowed to counter this with an own 7." << endl;
		cout	<< "  - Did the previous player play an 8, one has to skip his turn." << endl;

		cout	<< endl << " Press 'Enter' to continue.";
	} else {
		cout	<< " Willkommen!" << endl << endl;

		cout	<< " Dies ist New Old Mau-Mau, eine klassische Variante des bekannten Kartenspiels." << endl << endl;

		cout	<< " Ziel des Spiels ist es, alle Karten auf der Hand loszuwerden." << endl;
		cout	<< " Dies erreicht man, indem jeder Spieler reihum jeweils eine Karte ablegt." << endl;
		cout	<< " Wer zuerst keine Karten mehr hat, gewinnt und beendet damit das Spiel." << endl << endl;

		cout	<< " Die Regeln lauten dabei folgendermassen:" << endl;
		cout	<< "  - Die Karte die man ablegt muss entweder dieselbe Farbe (Pik, Herz, ...) oder" << endl;
		cout	<< "    denselben Wert (7, 10, Koenig, ...) wie die aktuelle offene Karte haben." << endl;
		cout	<< "  - \"Bube geht auf alles\": Ein Bube kann auf jede Karte gelegt werden ..." << endl;
		cout	<< "  - \"Bube auf Bube stinkt\": ... auf jede Karte, ausser einen anderen Buben." << endl;
		cout	<< "  - Setzt der vorige Spieler eine 7, muss man zwei Karten ziehen." << endl;
		cout	<< "  - Tritt dieser Fall ein, darf man keine Karte mehr ablegen." << endl;
		cout	<< "  - Ausserdem kann nicht mit einer eigenen 7 gekontert werden." << endl;
		cout	<< "  - Setzt der vorige Spieler eine 8, muss man aussetzen." << endl;

		cout	<< endl << " Druecke 'Enter', um zu bestaetigen.";
	}

	cin.get();
	clear();
	
	return;
}

void CMauMau::PrintLanguagePrompt( void ) {
	int input = 0;
	unsigned int tempLineCnt = 13;

	PrintHeader();

	cout	<< " Language/Sprache?" << endl;
	cout	<< "  [1] English" << endl;
	cout	<< "  [2] Deutsch" << endl;
	cout	<< " Your choice/deine Wahl: ";
	cin		>> input;
	cin.get();

	while ( input < EN || input > DE ) {
		cout	<< endl << " Sorry, this language is not supported." << endl;
		cout	<< " Diese Sprache wird leider nicht unterstuetzt." << endl;
		cout	<< " Choose another one/waehle eine andere: ";
		cin		>> input;
		cin.get();
		tempLineCnt -= 4;
	}

	language = input;

	if ( language == EN ) {
		cout	<< endl << " Good Choice!";
	} else {
		cout	<< endl << " Gute Wahl!";
	}

	for ( unsigned int i = 0; i < tempLineCnt; i++ ) {
		cout	<< endl;
	}

	if ( language == EN ) {
		cout	<< " Press 'Enter' to continue.";
	} else {
		cout	<< " Druecke 'Enter', um zu bestaetigen.";
	}

	cin.get();
	clear();
	
	return;
}

void CMauMau::PrintSettingsPage( void ) {
	srand( (unsigned int) time( 0 ) );
	string name = "";
	
	PrintHeader();
	
	if ( language == EN ) {
		cout	<< " Now you can set some preferences for the game." << endl << endl;

		cout	<< " First off, what's your name (or nickname)? ";
	} else {
		cout	<< " Nun kannst du ein paar Einstellungen fuer das Spiel durchfuehren." << endl << endl;

		cout	<< " Zuerst, wie lautet dein Vorname (Nickname geht auch)? ";
	}
	cin		>> name;
	cin.get();

	if ( language == EN ) {
		cout	<< " Ah! Nice to meet you, " << name << "!" << endl << endl;

		cout	<< " How many opponents do you want to play against (max. 7)? ";
	} else {
		cout	<< " Ah! Schoen, dich kennenzulernen, " << name << "!" << endl << endl;

		cout	<< " Gegen wie viele Gegner moechtest du denn spielen (max. 7)? ";
	}
	cin		>> countOfOpponents;
	cin.get();

	if ( countOfOpponents < 1 ) {
		countOfOpponents = 1;
	} else if ( countOfOpponents > 7 ) {
		countOfOpponents = 7;
	}

	if ( language == EN ) {
		cout	<< endl << " And how many cards do you want to be dealt per each hand?" << endl;
		cout	<< " For " << ( countOfOpponents + 1 ) << " players, ";
	} else {
		cout	<< endl << " Und wie viele Karten soll jeder zu Beginn auf die Hand bekommen?" << endl;
		cout	<< " Empfohlen fuer " << ( countOfOpponents + 1 ) << " Spieler sind ";
	}

	if ( countOfOpponents <= 1 ) {
		cout	<< "7";
	} else if ( countOfOpponents >= 4 ) {
		cout	<< "4";
	} else {
		cout	<< "5";
	}
	if ( language == EN ) {
		cout	<< " cards are recommended." << endl;
		cout	<< " How many cards? ";
	} else {
		cout	<< " Karten." << endl;
		cout	<< " Wie viele Karten? ";
	}
	cin		>> handSize;
	cin.get();
	cout	<< endl;

	// CPlayer objekts are being initialized
	players.push_back( CPlayer( true, 0 ) );
	for ( unsigned int i = 0; i < ( countOfOpponents ); i++ ) {
		players.push_back( CPlayer( false, ( i + 1 ) ) );
	}
	players.at( 0 ).SetName( name );
	
	// One player is randomly being chosen to get the first turn
	activePlayer = rand() % players.size();
	
	if ( language == EN ) {
		cout	<< " Great! Now everything is prepared." << endl << endl;
	} else {
		cout	<< " Super! Nun ist alles vorbereitet." << endl << endl;
	}
	
	if ( language == EN ) {
		if ( activePlayer == 0 ) {
			cout	<< " You were chosen to get the first turn." << endl;
		} else {
			cout	<< " Player" << ( activePlayer + 1 ) << " was chosen to get the first turn." << endl;
		}
	} else {
		if ( activePlayer == 0 ) {
			cout	<< " Du darfst den ersten Zug machen." << endl;
		} else {
			cout	<< " Spieler" << ( activePlayer + 1 ) << " darf den ersten Zug machen." << endl;
		}
	}

	for ( unsigned int i = 0; i < 4; i++ ) {
		cout	<< endl;
	}

	if ( language == EN ) {
		cout	<< " Press 'Enter' to continue.";
	} else {
		cout	<< " Druecke 'Enter', um zu bestaetigen.";
	}

	cin.get();
	clear();

	return;
}

void CMauMau::ShuffleCards( void ) {
	srand( (unsigned int) time( 0 ) );
	int size = drawingStack.size();
	
	for ( unsigned int i = 0; i < 200; i++ ) {
		do {
			random1 = rand() % ( size / 4 );
			random2 = ( rand() % ( size / 4 ) ) + ( size / 4 );
			random3 = ( rand() % ( size / 4 ) ) + ( size / 2 );
			random4 = ( rand() % ( size / 4 ) ) + ( ( size * 3 ) / 4 );
		} while ( RandomNumbersEqual() );

		for ( unsigned int j = 0; j < 4; j++ ) {
			switch ( j ) {
			case 0:
				random = random1;
				break;
			case 1:
				random = random2;
				break;
			case 2:
				random = random3;
				break;
			case 3:
				random = random4;
				break;
			default:
				break;
			}

			do {
				random5 = rand() % size;
			} while ( random == random5 );

			drawingStack.push_back( drawingStack.at( random ) );
			drawingStack.at( random ) = drawingStack.at( random5 );
			drawingStack.at( random5 ) = drawingStack.back();
			drawingStack.pop_back();
		}
	}

	return;
}

void CMauMau::StartGame( void ) {
#ifndef DEBUG
	PrintLanguagePrompt();

	PrintIntroduction();

	PrintSettingsPage();
#endif // !DEBUG

#ifdef DEBUG
	// CPlayer objekts are being initialized
	players.push_back( CPlayer( true, 0 ) );
	for ( unsigned int i = 0; i < ( countOfOpponents ); i++ ) {
		players.push_back( CPlayer( false, ( i + 1 ) ) );
	}
#endif // DEBUG

	// The deck is being filled
	for ( unsigned int i = SPADES; i <= CLUBS; i++ ) {
		for ( unsigned int j = 7; j <= ACE; j++ ) {
			drawingStack.push_back( CPlayingCard( i, j ) );
		}
	}

	ShuffleCards();

	Deal();
	
	// The topmost card is being revealed
	openStack.push_back( drawingStack.back() );
	drawingStack.pop_back();

	GameLoop();

	return;
}

void CMauMau::SwapStacks( void ) {
	CPlayingCard openCard = openStack.back();
	openStack.pop_back();

	for ( unsigned int i = 0; i < openStack.size(); i++ ) {
		drawingStack.push_back( openStack.back() );
		openStack.pop_back();
	}
	openStack.push_back( openCard );
	ShuffleCards();
	
	return;
}