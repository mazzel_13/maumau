// defining numbers to the suits and facing cards for better recognition in later use
#define SPADES		1
#define HEARTS		2
#define DIAMONDS	3
#define CLUBS		4
#define JACK		11
#define QUEEN		12
#define KING		13
#define ACE			14

// actual class declaration
#pragma once

class CPlayingCard {
public:
			CPlayingCard( void );
			CPlayingCard( int _suit, int _value );

			~CPlayingCard( void );

	int		GetSuit( void );
	int		GetValue( void );

	bool	IsValid( bool jackIsPlayed, int requestedSuit, CPlayingCard openCard );

private:
	int		suit;
	int		value;
};