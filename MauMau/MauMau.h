// define numbers to the different languages for better recognition in later use
#define EN 1
#define DE 2

// actual class declaration
#pragma once

class CMauMau {
public:
							CMauMau( void );

							~CMauMau( void );

	vector<CPlayer>			GetPlayers( void );
	vector<CPlayingCard>	GetDrawingStack( void );
	
	void					ExpandOpenStack( CPlayingCard card );
	void					ReduceDrawingStack( void );
	void					SetJack( bool state, int owner );
	void					SetRequestedSuit( int suit );
	void					SetSpecialCard( int card );

	void					PrintOneCard( CPlayingCard card );
	void					PrintStack( vector<CPlayingCard> stack );
	
private:
	bool					jackIsPlayed;
	int						jackOwner;
	int						language;
	int						random;
	int						random1;
	int						random2;
	int						random3;
	int						random4;
	int						random5;
	int						requestedSuit;
	int						specialCard;
	unsigned int			activePlayer;
	unsigned int			countOfOpponents;
	unsigned int			handSize;
	vector<CPlayer>			players;
	vector<CPlayingCard>	drawingStack;
	vector<CPlayingCard>	openStack;

	bool					GameStillRunning( void );
	bool					RandomNumbersEqual( void );
	void					AnnounceWinner( void );
	void					Deal( void );
	void					GameLoop( void );
	void					PrintGameBoard( void );
	void					PrintHeader( void );
	void					PrintIntroduction( void );
	void					PrintLanguagePrompt( void );
	void					PrintSettingsPage( void );
	void					ShuffleCards( void );
	void					StartGame( void );
	void					SwapStacks( void );
};

extern CMauMau game;