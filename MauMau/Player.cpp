#include "precompiled.h"

// Constructors
// -- do not use this default constructor!
CPlayer::CPlayer( void ) {
}

// -- always use this constructor instead!
CPlayer::CPlayer( bool _isHuman, int _id ) {
	isHuman = _isHuman;
	id = _id;

#ifdef DEBUG
	name = "Marcel";  
#endif // DEBUG

	lineCnt = 0;
}

// Destructor
CPlayer::~CPlayer( void ) {
}

// Getter
string CPlayer::GetName( void ) {
	return ( name );
}

vector<CPlayingCard> CPlayer::GetHand( void ) {
	return ( hand );
}

// Setter
void CPlayer::ExpandHand( CPlayingCard card ) {
	hand.push_back( card );
	
	return;
}

void CPlayer::SetName( string _name ) {
	name = _name;

	return;
}

// Remaining public methods
void CPlayer::Turn( bool jackIsPlayed, int jackOwner, int language, int requestedSuit, int specialCard, CPlayingCard openCard ) {
	lineCnt = 12;
	
	SpecialAction( jackOwner, language, requestedSuit, specialCard, openCard.GetValue() );

	if ( isHuman ) {
		bool inputValid = false;
		int input = 0;

		if ( specialCard == 0 || specialCard == JACK ) {
			if ( language == EN ) {
				cout	<< " Your options:" << endl;
				cout	<< "   [0] Draw a card" << endl;
			} else {
				cout	<< " Deine Optionen:" << endl;
				cout	<< "   [0] Eine Karte ziehen" << endl;
			}
			for ( unsigned int i = 0; i < hand.size(); i++ ) {
				if ( ( i + 1 ) < 10 ) {
					cout	<< " ";
				}
				cout	<< "  [" << i + 1 << "] ";
				if ( language == EN ) {
					cout	<< "Play card: \"";
				} else {
					cout	<< "Karte ablegen: \"";
				}
				PrintOneCard( hand.at( i ), language );
				cout	<< "\"" << endl;
			}
			if ( language == EN ) {
				cout	<< " What is your choice? ";
			} else {
				cout	<< " Was moechtest du tun? ";
			}
			cin		>> input;
			cin.get();

			while ( !inputValid && input != 0) {
				if ( input < 0 ) {
					if ( language == EN ) {
						cout	<< endl << " Come on, don't fool around..." << endl;
						cout	<< " Just enter an appropriate number: ";
					} else {
						cout	<< endl << " Ach komm, treib keine Spielchen..." << endl;
						cout	<< " Gib einfach einen vernuenftigen Wert ein: ";
					}
				} else if ( ( unsigned int ) input > hand.size() || !hand.at( input - 1 ).IsValid( jackIsPlayed, requestedSuit, openCard ) ) {
					if ( ( unsigned int ) input > hand.size() ) {
						if ( language == EN ) {
							cout	<< endl << " You don't even have that many cards in your hand." << endl;
						} else {
							cout	<< endl << " So viele Karten hast du gar nicht auf der Hand." << endl;
						}
					} else {
						if ( language == EN ) {
							cout	<< endl << " That turn wouldn't comply with the rules." << endl;
						} else {
							cout	<< endl << " Dieser Zug waere nicht regelkonform." << endl;
						}
					}
					if ( language == EN ) {
						cout	<< " Please choose another card: ";
					} else {
						cout	<< " Bitte waehle eine andere Karte aus: ";
					}
				} else {
					inputValid = true;
				}
				if ( !inputValid ) {
					cin		>> input;
					cin.get();
				}
			}

			PlayOneCard( input, language );
		}
	} else {
		bool playedOneCard = false;

		if ( language == EN ) {
			cout	<< endl << " Player" << ( id + 1 ) << " still has " << hand.size() << " card";
			if ( hand.size() > 1 ) {
				cout	<< "s";
			}
			cout	<< " in his hand." << endl << endl;
		} else {
			cout	<< endl << " Spieler" << ( id + 1 ) << " hat noch " << hand.size() << " Karte";
			if ( hand.size() > 1 ) {
				cout	<< "n";
			}
			cout	<< " auf der Hand." << endl << endl;
		}
		IncreaseLineCnt( 3 );

		if ( specialCard == 0 || specialCard == JACK ) {
			for ( unsigned int i = 0; i < hand.size() && !playedOneCard; i++ ) {
				if ( hand.at( i ).IsValid( jackIsPlayed, requestedSuit, openCard ) ) {
					if ( language == EN ) {
						cout	<< " Player" << ( id + 1 ) << " plays the card \"";
					} else {
						cout	<< " Spieler" << ( id + 1 ) << " legt die Karte \"";
					}
					PrintOneCard( hand.at( i ), language );
					if ( language == EN ) {
						cout	<< "\"." << endl;
					} else {
						cout	<< "\" ab." << endl;
					}
					IncreaseLineCnt( 1 );
					PlayOneCard( i + 1, language );
					playedOneCard = true;
				}
			}
			if ( !playedOneCard ) {
				DrawCards( 1 );
				if ( language == EN ) {
					cout	<< " Player" << ( id + 1 ) << " has to draw a card." << endl;
				} else {
					cout	<< " Spieler" << ( id + 1 ) << " muss eine Karte ziehen." << endl;
				}
				IncreaseLineCnt( 1 );
			}
		}

		ConfirmToProceed( language );
	}
	
	return;
}

// Remaining private methods
void CPlayer::ConfirmToProceed( int language ) {
	if ( lineCnt <= 22 ) {
		for ( unsigned int i = lineCnt; i <= 23; i++ ) {
			cout	<< endl;
		}
	} else {
		cout	<< endl;
	}

	if ( language == EN ) {
		cout	<< " Press 'Enter' to continue.";
	} else {
		cout	<< " Druecke 'Enter', um zu bestaetigen.";
	}
	
	cin.get();
	
	return;
}

void CPlayer::DrawCards( unsigned int cnt ) {
	for ( unsigned int i = 0; i < cnt; i++ ) {
		hand.push_back( game.GetDrawingStack().back() );
		game.ReduceDrawingStack();
	}
	
	return;
}

void CPlayer::IncreaseLineCnt( unsigned int cnt ) {
	for ( unsigned int i = 0; i < cnt; i++ ) {
		lineCnt++;
	}
	
	return;
}

void CPlayer::PlayOneCard( int card, int language ) {
	if ( card == 0 ) {
		DrawCards( 1 );
	} else {
		SpecialCard( hand.at( card - 1 ).GetValue(), language );
		game.ExpandOpenStack( hand.at( card - 1 ) );
		for ( int i = ( card - 1 ); ( unsigned int ) i < ( hand.size() - 1 ); i++ ) {
			hand.at( i ) = hand.at( i + 1 );
		}
		hand.pop_back();
	}

	return;
}

void CPlayer::PrintOneCard( CPlayingCard card, int language ) {
	if ( language == EN ) {
		switch ( card.GetValue() ) {
		case 7:
			cout	<< "Seven";
			break;
		case 8:
			cout	<< "Eight";
			break;
		case 9:
			cout	<< "Nine";
			break;
		case 10:
			cout	<< "Ten";
			break;
		case JACK:
			cout	<< "Jack";
			break;
		case QUEEN:
			cout	<< "Queen";
			break;
		case KING:
			cout	<< "King";
			break;
		case ACE:
			cout	<< "Ace";
			break;
		default:
			break;
		}
		cout	<< " of ";
		switch ( card.GetSuit() ) {
		case SPADES:
			cout	<< "Spades";
			break;
		case HEARTS:
			cout	<< "Hearts";
			break;
		case DIAMONDS:
			cout	<< "Diamonds";
			break;
		case CLUBS:
			cout	<< "Clubs";
			break;
		default:
			break;
		}
	} else {
		switch ( card.GetSuit() ) {
		case SPADES:
			cout	<< "Pik";
			break;
		case HEARTS:
			cout	<< "Herz";
			break;
		case DIAMONDS:
			cout	<< "Karo";
			break;
		case CLUBS:
			cout	<< "Kreuz";
			break;
		default:
			break;
		}
		cout	<< "-";
		switch ( card.GetValue() ) {
		case 7:
			cout	<< "Sieben";
			break;
		case 8:
			cout	<< "Acht";
			break;
		case 9:
			cout	<< "Neun";
			break;
		case 10:
			cout	<< "Zehn";
			break;
		case JACK:
			cout	<< "Bube";
			break;
		case QUEEN:
			cout	<< "Dame";
			break;
		case KING:
			cout	<< "Koenig";
			break;
		case ACE:
			cout	<< "Ass";
			break;
		default:
			break;
		}
	}
	
	return;
}

void CPlayer::SpecialAction( int jackOwner, int language, int requestedSuit, int specialCard, int valueOpenCard ) {

	if ( valueOpenCard != JACK ) {
		game.SetJack( false, game.GetPlayers().size() );
	}
	
	if ( specialCard == 7 ) {
		if ( isHuman ) {
			if ( language == EN ) {
				cout	<< endl << " Unfortunately, you have to draw two cards." << endl;
			} else {
				cout	<< endl << " Du musst leider zwei Karten ziehen." << endl;
			}
			IncreaseLineCnt( 2 );
			ConfirmToProceed( language );
		} else {
			if ( language == EN ) {
				cout	<< endl << " Player" << ( id + 1 ) << " has to draw two cards." << endl;
			} else {
				cout	<< endl << " Spieler" << ( id + 1 ) << " muss zwei Karten ziehen." << endl;
			}
			IncreaseLineCnt( 2 );
		}
		DrawCards( 2 );
		game.SetSpecialCard( 0 );
	} else if ( specialCard == 8 ) {
		if ( isHuman ) {
			if ( language == EN ) {
				cout	<< endl << " Unfortunately, you have to skip your turn." << endl;
			} else {
				cout	<< endl << " Du musst leider aussetzen." << endl;
			}
			IncreaseLineCnt( 2 );
			ConfirmToProceed( language );
		} else {
			if ( language == EN ) {
				cout	<< endl << " Player" << ( id + 1 ) << " has to skip his turn." << endl;
			} else {
				cout	<< endl << " Spieler" << ( id + 1 ) << " muss aussetzen." << endl;
			}
			IncreaseLineCnt( 2 );
		}
		game.SetSpecialCard( 0 );
	}/* else if ( specialCard == 9 ) {
		;
	}*/ else if ( specialCard == JACK ) {
		if ( jackOwner != 0 ) {
			if ( language == EN ) {
				cout	<< " Player" << ( jackOwner + 1 ) << " changed the suit to \"";
			} else {
				cout	<< " Spieler" << ( jackOwner + 1 ) << " wuenscht sich die Farbe \"";
			}
		} else {
			if ( language == EN ) {
				cout	<< " You changed the suit to \"";
			} else {
				cout	<< " Du wuenschst dir die Farbe \"";
			}
		}
		switch ( requestedSuit ) {
		case SPADES:
			if ( language == EN ) {
				cout	<< "Spades\"." << endl << endl;
			} else {
				cout	<< "Pik\"." << endl << endl;
			}
			IncreaseLineCnt( 2 );
			break;
		case HEARTS:
			if ( language == EN ) {
				cout	<< "Hearts\"." << endl << endl;
			} else {
				cout	<< "Herz\"." << endl << endl;
			}
			IncreaseLineCnt( 2 );
			break;
		case DIAMONDS:
			if ( language == EN ) {
				cout	<< "Diamonds\"." << endl << endl;
			} else {
				cout	<< "Karo\"." << endl << endl;
			}
			IncreaseLineCnt( 2 );
			break;
		case CLUBS:
			if ( language == EN ) {
				cout	<< "Clubs\"." << endl << endl;
			} else {
				cout	<< "Kreuz\"." << endl << endl;
			}
			IncreaseLineCnt( 2 );
			break;
		default:
			break;
		}
	}/* else if ( specialCard == ACE ) {
		;
	}*/
	
	return;
}

void CPlayer::SpecialCard( int cardValue, int language ) {
	if ( cardValue == 7 ) {
		game.SetSpecialCard( 7 );
	} else if ( cardValue == 8 ) {
		game.SetSpecialCard( 8 );
	}/* else if ( cardValue == 9 ) {
		game.SetSpecialCard( 9 );
	}*/ else if ( cardValue == JACK && isHuman ) {
		int requestedSuit = 0;
		
		game.SetSpecialCard( JACK );
		if ( language == EN ) {
			cout	<< endl << " You may now choose: " << endl;
			cout	<< "  [1] Spades " << endl;
			cout	<< "  [2] Hearts " << endl;
			cout	<< "  [3] Diamonds " << endl;
			cout	<< "  [4] Clubs " << endl;
			cout	<< " Change the suit to: ";
		} else {
			cout	<< endl << " Du darfst waehlen: " << endl;
			cout	<< "  [1] Pik " << endl;
			cout	<< "  [2] Herz " << endl;
			cout	<< "  [3] Karo " << endl;
			cout	<< "  [4] Kreuz " << endl;
			cout	<< " Welche Farbe wuenschst du dir? ";
		}
		cin		>> requestedSuit;
		cin.get();
		IncreaseLineCnt( 7 );
		while ( requestedSuit > 4 || requestedSuit < 1 ) {
			if ( language == EN ) {
				cout	<< " You may have hit the wrong key." << endl;
				cout	<< " Choose another suit: ";
			} else {
				cout	<< " Du hast dich wohl vertippt." << endl;
				cout	<< " Waehle eine andere Farbe: ";
			}
			IncreaseLineCnt( 2 );
			cin		>> requestedSuit;
		}
		game.SetRequestedSuit( requestedSuit );
		game.SetJack( true, id );
	} else if ( cardValue == JACK && !isHuman ) {
		int spadesCnt = 0;
		int heartsCnt = 0;
		int diamondsCnt = 0;
		int clubsCnt = 0;
		int max = 0;
		
		game.SetSpecialCard( JACK );
		for ( unsigned int i = 0; i < hand.size(); i++ ) {
			if ( hand.at( i ).GetSuit() == SPADES ) {
				clubsCnt++;
			} else if ( hand.at( i ).GetSuit() == HEARTS ) {
				spadesCnt++;
			} else if ( hand.at( i ).GetSuit() == DIAMONDS ) {
				heartsCnt++;
			} else if ( hand.at( i ).GetSuit() == CLUBS ) {
				diamondsCnt++;
			}
		}

		max = ( max > spadesCnt ) ? max : spadesCnt;
		max = ( max > heartsCnt ) ? max : heartsCnt;
		max = ( max > diamondsCnt ) ? max : diamondsCnt;
		max = ( max > clubsCnt ) ? max : clubsCnt;
		
		if ( language == EN ) {
			cout	<< " And changes the suit to \"";
		} else {
			cout	<< " Und wuenscht sich die Farbe \"";
		}
		if ( spadesCnt == max ) {
			game.SetRequestedSuit( SPADES );
			if ( language == EN ) {
				cout	<< "Spades\"." << endl;
			} else {
				cout	<< "Pik\"." << endl;
			}
		} else if ( heartsCnt == max ) {
			game.SetRequestedSuit( HEARTS );
			if ( language == EN ) {
				cout	<< "Hearts\"." << endl;
			} else {
				cout	<< "Herz\"." << endl;
			}
		} else if ( diamondsCnt == max ) {
			game.SetRequestedSuit( DIAMONDS );
			if ( language == EN ) {
				cout	<< "Diamonds\"." << endl;
			} else {
				cout	<< "Karo\"." << endl;
			}
		} else if ( clubsCnt == max ) {
			game.SetRequestedSuit( CLUBS );
			if ( language == EN ) {
				cout	<< "Clubs\"." << endl;
			} else {
				cout	<< "Kreuz\"." << endl;
			}
		}
		IncreaseLineCnt( 1 );

		game.SetJack( true, id );
	}/* else if ( cardValue == ACE ) {
		game.SetSpecialCard( ACE );
	}*/ else {
		game.SetSpecialCard( 0 );
	}
	
	return;
}