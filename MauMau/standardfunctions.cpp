#include "precompiled.h"

void clear( void ) {
	COORD topLeft = { 0, 0 };
	HANDLE console = GetStdHandle( STD_OUTPUT_HANDLE );
	CONSOLE_SCREEN_BUFFER_INFO screen;
	DWORD written;

	GetConsoleScreenBufferInfo( console, &screen );
	FillConsoleOutputCharacterA( console, ' ', screen.dwSize.X * screen.dwSize.Y, topLeft, &written	);
	FillConsoleOutputAttribute(	console, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE,
								screen.dwSize.X * screen.dwSize.Y, topLeft, &written );
	SetConsoleCursorPosition( console, topLeft );
	
	return;
}

void endProgram( void ) {
	cout	<< endl << endl << "  +------------------------------------+";
	cout	<< endl << "  | Press \"Enter\" to quit the program. |";
	cout	<< endl << "  +------------------------------------+ " << endl;
	// cin.get();
	cin.get();
	
	return;
}