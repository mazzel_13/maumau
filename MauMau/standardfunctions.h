// defines
#define PI ( atan( 1.0 ) * 4.0 )	// Pi: ratio between circumference and diameter of a circle
#define E 2.718281828459045			// e: base of the natrual logarithm
#define C 299792.458				// c: speed of light in km/s

// functions
void	clear( void );
void	endProgram( void );