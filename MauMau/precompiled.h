// DEBUG mode
#define DEBUG

// used namespaces
using namespace std;

// default includes from standard library
#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>

// structs

// classes
#include "PlayingCard.h"
#include "Player.h"
#include "MauMau.h"

// includes of remaining headers
#include "standardfunctions.h"